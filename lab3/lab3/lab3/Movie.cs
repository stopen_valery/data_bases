﻿using System;
using System.Collections.Generic;

#nullable disable

namespace lab3
{
    public partial class Movie
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Genre { get; set; }
        public string Country { get; set; }
        public int Duration { get; set; }
        public int? DirectorId { get; set; }

        public virtual Director Director { get; set; }
    }
}
