﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace lab3.MVC
{
    class Model
    {
        private stopen_dbContext db;
        public Model()
        {
            db = new stopen_dbContext();
        }
      

        #region movies
        public List<Movie> movie_print()
        {
            var movies = db.Movies.ToList();
            return movies;
        }
        public Movie movie_get_by_id(int movie_id)
        {
            var movie = db.Movies.FirstOrDefault(m => m.Id == movie_id);
            return movie;
        }
        public List<Movie> movie_print_hash1()
        {
            var movies = db.Movies.Where(m => m.Country == "Fpu").OrderBy(m => m.Name).ToList();
            return movies;
        }
        public List<Movie> movie_print_hash2()
        {
            var movies = db.Movies.Where(m => m.Country == "Vhc").Where(m => EF.Functions.Like(m.Duration.ToString(), "%1%")).ToList();
            return movies;
        }
        public int movie_add(Movie movie)
        {
            db.Movies.Add(movie);
            db.SaveChanges();
            var m = db.Movies.Max(m => m.Id);
            return m;
        }
        public bool movie_delete(int movie_id)
        {
            var movie = movie_get_by_id(movie_id);
            if(movie == null)
            {
                return false;
            }
            db.Movies.Remove(movie);
            db.SaveChanges();
            return true;
        }
        public void movie_edit(Movie movie)
        {
            var m = movie_get_by_id(movie.Id);
            m = movie;
            db.SaveChanges();
        }
        #endregion

        #region directors
        public List<Director> director_print()
        {
            var directors = db.Directors.ToList();
            return directors;
        }
        public Director director_get_by_id(int dir_id)
        {
            var director = db.Directors.FirstOrDefault(d => d.Id == dir_id);
            return director;
        }
        public int director_add(Director director)
        {
            db.Directors.Add(director);
            db.SaveChanges();
            var d = db.Directors.Max(d => d.Id);
            return d;
        }
        
        public bool director_delete(int director_id)
        {
            var director = director_get_by_id(director_id);
            if (director == null)
            {
                return false;
            }
            db.Directors.Remove(director);
            db.SaveChanges();
            return true;
        }
        public void director_edit(Director director)
        {
            var d = director_get_by_id(director.Id);
            d = director;
            db.SaveChanges();
        }
        #endregion
        

        #region awards
        public List<Award> award_print()
        {
            var awards = db.Awards.ToList();
            return awards;
        }
        public Award award_get_by_id(int award_id)
        {
            var award = db.Awards.FirstOrDefault(a => a.Id == award_id); 
            return award;
        }
        public string award_print_brin1()
        {
            var ret = db.Nominations.Include(m => m.Movie).Include(a => a.Award).Where(a => a.Award.Year == 2020);
            string movie_awards = "";
            foreach (var r in ret)
            {
                movie_awards += r.Id.ToString();
                movie_awards += ". ";
                movie_awards += r.MovieId.ToString();
                movie_awards += ". Name: ";
                movie_awards += r.Movie.Name;
                movie_awards += " <-----> ";
                movie_awards += r.AwardId.ToString();
                movie_awards += ". Category: ";
                movie_awards += r.Award.Category;
                movie_awards += " Year: ";
                movie_awards += r.Award.Year.ToString();
                movie_awards += '\n';
            }
            return movie_awards;
        }
        public int award_print_brin2()
        {
            var ret = db.Awards.Where(a => a.Year == 2020).Count();
            return ret;
        }
            public int award_add(Award award)
        {
            db.Awards.Add(award);
            db.SaveChanges();
            var a = db.Awards.Max(a => a.Id);
            return a;
        }
        
        public bool award_delete(int award_id)
        {
            var award = award_get_by_id(award_id);
            if (award == null)
            {
                return false;
            }
            db.Awards.Remove(award);
            db.SaveChanges();
            return true;
        }
        
        public void award_edit(Award award)
        {
            var a = award_get_by_id(award.Id);
            a = award;
            db.SaveChanges();
        }
        #endregion
        
        #region movie_award
        public string movie_award_print()
        {
            var ret = db.Nominations.Include(m => m.Movie).Include(a => a.Award).ToList();
            string movie_awards = "";
            foreach(var r in ret)
            {
                movie_awards += r.Id.ToString();
                movie_awards += ". ";
                movie_awards += r.MovieId.ToString();
                movie_awards += ". Name: ";
                movie_awards += r.Movie.Name;
                movie_awards += " <-----> ";
                movie_awards += r.AwardId.ToString();
                movie_awards += ". Category: ";
                movie_awards += r.Award.Category;
                movie_awards += '\n';
            }
            return movie_awards;
        }
        public string movie_award_get_by_id(int nom_id)
        {
            var ret = db.Nominations.Include(m => m.Movie).Include(a => a.Award).FirstOrDefault(n => n.Id == nom_id);
            string movie_awards = "";
            if (ret != null)
            {
                movie_awards += ret.Id.ToString();
                movie_awards += ". ";
                movie_awards += ret.MovieId.ToString();
                movie_awards += ". Name: ";
                movie_awards += ret.Movie.Name;
                movie_awards += " <-----> ";
                movie_awards += ret.AwardId.ToString();
                movie_awards += ". Category: ";
                movie_awards += ret.Award.Category;
                movie_awards += '\n';
            }
            return movie_awards;
        }
        public void movie_award_add(Nomination nomination)
        {
            db.Nominations.Add(nomination);
            db.SaveChanges();
        }
        public bool movie_award_delete(int nom_id)
        {
            var nom = db.Nominations.FirstOrDefault(n => n.Id == nom_id);
            if (nom == null)
            {
                return false;
            }
            db.Nominations.Remove(nom);
            db.SaveChanges();
            return true;
        }

        public void backup()
        {
            string sProcess = @"C:\windows\system32\cmd.exe";
            string command = "C:/\"Program Files\"/PostgreSQL/13/bin/pg_dump.exe --file C:/Users/user/Documents/database/lab3.sql  --host localhost --port 5432 --username postgres --password --verbose --format=c --blobs stopen_db\n";
            string cmd = String.Format(" /k {0}{1}{2} x86", "\"", command, "\"");
            Process.Start(sProcess, cmd);
        }
        #endregion
    }
}
