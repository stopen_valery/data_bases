﻿using System;
using System.Collections.Generic;

namespace lab3.MVC
{
    class View
    {
        public string entity ()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Menu:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.Movie\n2.Director\n3.Award\n4.Movies - Awards\n5.Hash Indexes\n6.Brin Indexes\n7.Exit");
            return Console.ReadLine();
        }
        #region movie
        public string movie()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Movie Menu:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.Print list of movies\n2.Print movie by ID\n3.Add movie\n4.Delete movie by ID\n5.Edit movie by ID\n6.Choose another entity");
            return Console.ReadLine();
        }
        public string movie_get_name()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Movie name:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string movie_get_genre()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Movie genre:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string movie_get_country()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Movie country:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string movie_get_duration()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Movie duration:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string movie_get_dirid()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Movie director ID:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public void print_movie(Movie m)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Movies:");
            Console.ForegroundColor = ConsoleColor.Gray;
            
            Console.WriteLine(m.Id.ToString() + ". Name: " + m.Name + " Country: " + m.Country + " Duration: " + m.Duration.ToString() + " Genre: " + m.Genre + " Director ID: " + m.DirectorId.ToString());

        }
        public void print_movies(List<Movie> movies)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("A list of movies:");
            Console.ForegroundColor = ConsoleColor.Gray;
            foreach (Movie m in movies)
            {
                Console.WriteLine(m.Id.ToString() + ". Name: " + m.Name + " Country: " + m.Country + " Duration: " + m.Duration.ToString() + " Genre: " + m.Genre + " Director ID: " + m.DirectorId.ToString());
            }
        }
        #endregion
        #region director
        public string director()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Director Menu:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.Print list of directors\n2.Print director by ID\n3.Add director\n4.Delete director by ID\n5.Edit director by ID\n6.Choose another entity");
            return Console.ReadLine();
        }
        public string director_get_name()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Director name:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string director_get_country()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Director county of birth:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public void print_directors(List<Director> directors)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("A list of directors:");
            Console.ForegroundColor = ConsoleColor.Gray;
            foreach (Director d in directors)
            {
                Console.WriteLine(d.Id.ToString() + ". Name: " + d.Name + " Country: " + d.CountryOfBirth);
            }
        }
        public void print_director(Director d)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Director:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(d.Id.ToString() + ". Name: " + d.Name + " Country: " + d.CountryOfBirth);

        }
        #endregion
        #region award
        public string award()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Award Menu:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.Print list of awards\n2.Print award by ID\n3.Add award\n4.Delete award by ID\n5.Edit award by ID\n6. Choose another entity");
            string a = Console.ReadLine();
            return a;
        }
        public string award_get_category()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Award category:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }

        public string award_get_year()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Award year:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public void print_awards(List<Award> awards)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("A list of awards:");
            Console.ForegroundColor = ConsoleColor.Gray;
            foreach (Award a in awards)
            {
                Console.WriteLine(a.Id.ToString() + ". Category: " + a.Category + " Year: " + a.Year.ToString());
            }
        }
        public void print_award(Award a)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Award:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(a.Id.ToString() + ". Category: " + a.Category + " Year: " + a.Year.ToString());
        }
        #endregion
        #region movie_award
        public string movie_award()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Movies-Awards Menu:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.Print connections\n2.Get connection by ID\n3.Add connection\n4.Delete connection\n5.Choose another entity");
            string a = Console.ReadLine();
            return a;
        }
        public string get_movie_id ()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Input ID of movie:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string get_award_id()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Input ID of award:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }

        public void print_movie_awards(string movie_awards)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("A list of movie-award connections:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(movie_awards);
        }
        public void print_movie_award(string movie_award)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Nomination");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(movie_award);            
        }
        #endregion
        #region hash_index
        public string hash_index()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Hash Index Menu:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.Print list of movies with country Fpu ordered by name\n2.Print list of movies with country Vhc filtered by duretion\n3.Choose another entity");
            return Console.ReadLine();
        }
        #endregion
        #region brin_index
        public string brin_index()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Brin Index Menu:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.Print list of awards with year 2020 group by movie\n2.Print number of awards with year 2020\n3.Choose another entity");
            return Console.ReadLine();
        }
        public void brin_print(string brin)
        {
            Console.WriteLine(brin);
        }
            #endregion
        public string get_id()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Input ID of entity:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }


        #region errors
        public void err_wrong_entity ()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"The entity with such a number does not exist or you've entered a string");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public void err_wrong_option()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"The option with such a number does not exis or you've entered a string");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public void err_empty_table(string entity)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(entity + " table is empty");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public void err_wrong_ID (string entity)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(entity + " with ID does not exist or you've entered a string");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public void err_empty (string entity)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(entity + " cannot be empty");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public void err_number(string entity)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine( entity + " shold be a number");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        #endregion
        #region successfull
        public void successfull_operation (string entity, int ID, string operation)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine( entity + " with ID " + ID + " "+ operation + "  successfully");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public void successfull_connection()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("New connection added successfully");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        #endregion
    }
}
