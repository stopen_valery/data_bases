﻿using System;
using System.Collections.Generic;

namespace lab3.MVC
{
    class Controller
    {
        Model model = new Model();
        View view = new View();
        
        public int entity_menu()
        {
            model.backup();
            while (1 == 1)
            {
                int entity = 0;
                Int32.TryParse(view.entity(), out entity);
                if(entity == 1)
                {
                    movie_menu();
                    break;
                }
                else if (entity == 2)
                {
                    director_menu();
                    break;
                }
                else if (entity == 3)
                {
                    award_menu();
                    break;
                }
                else if (entity == 4)
                {
                    movie_award_menu();
                    break;
                }
                else if (entity == 5)
                {
                    hash_menu();
                    break;
                }
                else if (entity == 6)
                {
                    brin_menu();
                    break;
                }
                else if (entity == 7)
                {
                    return 1;
                }
                else
                {
                    view.err_wrong_entity();
                } 
            }
            return 0;
        }
        
        #region movie
        private void movie_menu()
        {
            while (1 == 1)
            {
                int movie = 0;
                Int32.TryParse(view.movie(), out movie);
                if (movie == 1)
                {
                    List<Movie> movies = model.movie_print();
                    if (movies.Count == 0)
                    {
                        view.err_empty_table("Movies");
                    }
                    else
                    {
                        view.print_movies(movies);
                    }
                }
                
                else if (movie == 2)
                {
                    int id = 0;
                    Int32.TryParse(view.get_id(), out id);
                    Movie movies = model.movie_get_by_id(id);
                    if (movies == null)
                    {
                        view.err_wrong_ID("Movie ");
                    }
                    else
                    {
                        view.print_movie(movies);
                    }
                }
                else if (movie == 3)
                {
                    Movie m = new Movie();
                    m.Name = view.movie_get_name();
                    while(m.Name.Length == 0)
                    {
                        view.err_empty("Movie name");
                        m.Name = view.movie_get_name();
                    }
                    m.Genre = view.movie_get_genre();
                    while (m.Genre.Length == 0)
                    {
                        view.err_empty("Movie genre");
                        m.Genre = view.movie_get_genre();
                    }
                    m.Country = view.movie_get_country();
                    while (m.Country.Length == 0)
                    {
                        view.err_empty("Movie country");
                        m.Country = view.movie_get_country();
                    }
                    int duration = 0;
                    string s_duration = view.movie_get_duration();
                    while (!Int32.TryParse(s_duration, out duration))
                    {
                        view.err_number("Movie duration");
                        s_duration = view.movie_get_duration();
                    }
                    m.Duration = duration;
                    int dir_id = 0;
                    string s_dir_id = view.movie_get_dirid();
                    Int32.TryParse(s_dir_id, out dir_id);
                    while (model.director_get_by_id(dir_id) == null)
                    {
                        view.err_wrong_ID("Director ");
                        s_dir_id = view.movie_get_dirid();
                        Int32.TryParse(s_dir_id, out dir_id);
                    }
                    m.DirectorId = dir_id;
                    int new_id =  model.movie_add(m);
                    view.successfull_operation("Movie", new_id, "added");
                }
                else if (movie == 4)
                {
                    int id = 0;
                    Int32.TryParse(view.get_id(), out id);
                    bool del = model.movie_delete(id);
                    if (del == false)
                    {
                        view.err_wrong_ID("Movie ");
                    }
                    else
                    {
                        view.successfull_operation("Movie", id, "deleted");
                    }
                }
                else if (movie == 5)
                {
                    int id = 0;
                    Int32.TryParse(view.get_id(), out id);
                    if (model.movie_get_by_id(id) == null)
                    {
                        view.err_wrong_ID("Movie ");
                    }
                    else
                    {
                        Movie m = new Movie();
                        m.Name = view.movie_get_name();
                        while (m.Name.Length == 0)
                        {
                            view.err_empty("Movie name");
                            m.Name = view.movie_get_name();
                        }
                        m.Genre = view.movie_get_genre();
                        while (m.Genre.Length == 0)
                        {
                            view.err_empty("Movie genre");
                            m.Genre = view.movie_get_genre();
                        }
                        m.Country = view.movie_get_country();
                        while (m.Country.Length == 0)
                        {
                            view.err_empty("Movie country");
                            m.Country = view.movie_get_country();
                        }
                        int duration = 0;
                        string s_duration = view.movie_get_duration();
                        while (!Int32.TryParse(s_duration, out duration))
                        {
                            view.err_number("Movie duration"); 
                            s_duration = view.movie_get_duration();
                        }
                        m.Duration = duration;
                        int dir_id = 0;
                        string s_dir_id = view.movie_get_dirid();
                        Int32.TryParse(s_dir_id, out dir_id);
                        while (model.director_get_by_id(dir_id) == null)
                        {
                            view.err_wrong_ID("Director ");
                            s_dir_id = view.movie_get_dirid();
                            Int32.TryParse(s_dir_id, out dir_id);
                        }
                        m.DirectorId = dir_id;
                        model.movie_edit(m);
                        view.successfull_operation("Movie", id, "edited");
                    }
                }
                else if (movie == 6)
                {
                    break;
                }
                else
                {
                    view.err_wrong_option();
                }
            }
        }
        #endregion
        #region director
        private void director_menu()
        {
            while (1 == 1)
            {
                int director = 0;
                Int32.TryParse(view.director(), out director);
                if(director == 1)
                {
                    List<Director> directors = model.director_print();
                    if (directors.Count == 0)
                    {
                        view.err_empty_table("Director");
                    }
                    else
                    {
                        view.print_directors(directors);
                    }
                }
                else if(director == 2)
                {
                    int id = 0;
                    Int32.TryParse(view.get_id(), out id);
                    Director directors = model.director_get_by_id(id);
                    if (directors == null)
                    {
                        view.err_wrong_ID("Director");
                    }
                    else
                    {
                        view.print_director(directors);
                    }
                }
                else if(director == 3)
                {
                    Director d = new Director();
                    d.Name = view.director_get_name();
                    while (d.Name.Length == 0)
                    {
                        view.err_empty("Director name");
                        d.Name = view.director_get_name();
                    }
                    d.CountryOfBirth = view.director_get_country();
                    while (d.CountryOfBirth.Length == 0)
                    {
                        view.err_empty("Director country");
                        d.CountryOfBirth = view.director_get_country();
                    }
                    int new_id = model.director_add(d);
                    view.successfull_operation("Director", new_id, "added");
                }
                else if(director == 4)
                {
                    int id = 0;
                    Int32.TryParse(view.get_id(), out id);
                    bool del = model.director_delete(id);
                    if (del == false)
                    {
                        view.err_wrong_ID("Director");
                    }
                    else
                    {
                        view.successfull_operation("Director", id, "deleted");
                    }
                }
                else if(director == 5)
                {
                    int id = 0;
                    Int32.TryParse(view.get_id(), out id);
                    if (model.director_get_by_id(id) == null)
                    {
                        view.err_wrong_ID("Director");
                    }
                    else
                    {
                        Director d = new Director();
                        d.Name = view.director_get_name();
                        while (d.Name.Length == 0)
                        {
                            view.err_empty("Director name");
                            d.Name = view.director_get_name();
                        }
                        d.CountryOfBirth = view.director_get_country();
                        while (d.CountryOfBirth.Length == 0)
                        {
                            view.err_empty("Director country");
                            d.CountryOfBirth = view.director_get_country();
                        }
                        model.director_edit(d);
                        view.successfull_operation("Director", id, "edited") ;
                    }
                }
                else if (director == 6)
                {
                    break;
                }
                else
                {
                    view.err_wrong_option();

                }
            }
        }
        #endregion
        #region award
        private void award_menu()
        {
            while (1 == 1)
            {
                int award = 0;
                Int32.TryParse(view.award(), out award);
                if (award == 1)
                {
                    List <Award> awards = model.award_print();
                    if (awards.Count == 0)
                    {
                        view.err_empty_table("Awards");
                    }
                    else
                    {
                        view.print_awards(awards);
                    }
                }
                else if (award == 2)
                {
                    int id = 0;
                    Int32.TryParse(view.get_id(), out id);
                    Award awards = model.award_get_by_id(id);
                    if (awards == null)
                    {
                        view.err_wrong_ID("Award");
                    }
                    else
                    {
                        view.print_award(awards);
                    }
                }
                else if (award == 3)
                {
                    Award a = new Award();
                    a.Category = view.award_get_category();
                    while (a.Category.Length == 0)
                    {
                        view.err_empty("Award category");
                        a.Category = view.award_get_category();
                    }
                    int year = 0;
                    string y = view.award_get_year();
                    while (!Int32.TryParse(y, out year))
                    {
                        view.err_number("Award year");
                        y = view.award_get_year();
                    }
                    a.Year = year;
                    int new_id = model.award_add(a);
                    view.successfull_operation("Award", new_id, "added");
                }
                else if (award == 4)
                {
                    int id = 0;
                    Int32.TryParse(view.get_id(), out id);
                    bool del = model.award_delete(id);
                    if (del == false)
                    {
                        view.err_wrong_ID("Award");
                    }
                    else
                    {

                        view.successfull_operation("Award", id, "deleted");
                    }
                }
                else if (award == 5)
                {
                    int id = 0;
                    Int32.TryParse(view.get_id(), out id);
                    if (model.award_get_by_id(id) == null)
                    {
                        view.err_wrong_ID("Award");
                    }
                    else
                    {
                        Award a = new Award();
                        a.Category = view.award_get_category();
                        while (a.Category.Length == 0)
                        {
                            view.err_empty("Award category");
                            a.Category = view.award_get_category();
                        }
                        int year = 0;
                        string y = view.award_get_year();
                        while (!Int32.TryParse(y, out year))
                        {
                            view.err_number("Award year");
                            y = view.award_get_year();
                        }
                        a.Year = year;
                        model.award_edit(a);
                        view.successfull_operation("Award", id, "edited");
                    }
                }
                else if (award == 6)
                {
                    break;
                }
                else
                {
                    view.err_wrong_option();
                }
            }
        }
        #endregion
        
        #region movie_award
        private void movie_award_menu()
        {
            while (1 == 1)
            {
                int movie_award = 0;
                Int32.TryParse(view.movie_award(), out movie_award);
                if (movie_award == 1)
                {
                    string movies_awards = model.movie_award_print();
                    if (movies_awards.Length == 0)
                    {
                        view.err_empty_table("Movie - Awards");
                    }
                    else
                    {
                        view.print_movie_awards(movies_awards);
                    }
                }
                else if (movie_award == 2)
                {
                    int id = 0;
                    Int32.TryParse(view.get_id(), out id);
                    string nominations = model.movie_award_get_by_id(id);
                    if (nominations.Length == 0)
                    {
                        view.err_wrong_ID("Nomination");
                    }
                    else
                    {
                        view.print_movie_award(nominations);
                    }
                }
                else if (movie_award == 3)
                {
                    int movie_id = 0;
                    Int32.TryParse(view.get_movie_id(), out movie_id);
                    while (model.movie_get_by_id(movie_id) == null)
                    {
                        view.err_wrong_ID("Movie");
                        Int32.TryParse(view.get_movie_id(), out movie_id);
                    }
                    int award_id = 0;
                    Int32.TryParse(view.get_award_id(), out award_id);
                    while (model.award_get_by_id(award_id) == null)
                    {
                        view.err_wrong_ID("Award");
                        Int32.TryParse(view.get_award_id(), out award_id);
                    }
                    Nomination nomination = new Nomination();
                    nomination.MovieId = movie_id;
                    nomination.AwardId = award_id;
                    model.movie_award_add(nomination);
                    view.successfull_connection();
                }
                
                else if (movie_award == 4)
                {
                    int id = 0;
                    Int32.TryParse(view.get_id(), out id);
                    bool del = model.movie_award_delete(id);
                    if (del == false)
                    {
                        view.err_wrong_ID("Nominations");
                    }
                    else
                    {

                        view.successfull_operation("Nomination", id, "deleted");
                    }
                }

                else if (movie_award == 5)
                {
                    break;
                }
                else
                {
                    view.err_wrong_option();
                }
            }
        }
        #endregion
        private void hash_menu()
        {
            while (1 == 1)
            {
                int hash = 0;
                Int32.TryParse(view.hash_index(), out hash);
                if (hash == 1)
                {
                    List<Movie> movies = model.movie_print_hash1();
                    view.print_movies(movies);
                }
                else if (hash == 2)
                {
                    List<Movie> movies = model.movie_print_hash2();
                    view.print_movies(movies);
                }
                else if (hash == 3)
                {
                    break;
                }
                else
                {
                    view.err_wrong_option();
                }
            }
        }
        private void brin_menu()
        {
            while (1 == 1)
            {
                int brin = 0;
                Int32.TryParse(view.brin_index(), out brin);
                if (brin == 1)
                {
                    string awards = model.award_print_brin1();
                    view.brin_print(awards);
                }
                else if (brin == 2) 
                {
                    int awards = model.award_print_brin2();
                    view.brin_print(awards.ToString());
                }
                else if (brin == 3)
                {
                    break;
                }
                else
                {
                    view.err_wrong_option();
                }
            }
        }
    }
}
