﻿using System;
using System.Collections.Generic;

#nullable disable

namespace lab3
{
    public partial class Award
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public int Year { get; set; }
    }
}
