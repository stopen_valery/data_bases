﻿using System;
using System.Collections.Generic;

#nullable disable

namespace lab3
{
    public partial class Nomination
    {
        public int AwardId { get; set; }
        public int MovieId { get; set; }

        public int Id { get; set; }

        public virtual Award Award { get; set; }
        public virtual Movie Movie { get; set; }
    }
}
