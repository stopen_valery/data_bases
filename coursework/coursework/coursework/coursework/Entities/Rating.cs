﻿using System;
using System.Collections.Generic;

#nullable disable

namespace coursework
{
    public partial class Rating
    {
        public int Id { get; set; }
        public int AvgNum { get; set; }
        public int NumOfRates { get; set; }
        public int? MovieId { get; set; }

        public virtual Movie Movie { get; set; }
    }
}
