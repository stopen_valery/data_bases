﻿using System;
using System.Collections.Generic;

#nullable disable

namespace coursework
{
    public partial class Director
    {
        public Director()
        {
            Movies = new HashSet<Movie>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string PlaceOfBirth { get; set; }
        public bool? IsAccessible { get; set; }

        public virtual ICollection<Movie> Movies { get; set; }
    }
}
