﻿using System;
using System.Collections.Generic;

#nullable disable

namespace coursework
{
    public partial class Movie
    {
        public Movie()
        {
            Ratings = new HashSet<Rating>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public int Duration { get; set; }
        public string Genre { get; set; }
        public string Country { get; set; }
        public int? DirectorId { get; set; }
        public bool IsAccessible { get; set; }

        public virtual Director Director { get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }
    }
}
