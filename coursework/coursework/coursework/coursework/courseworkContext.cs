﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace coursework
{
    public partial class courseworkContext : DbContext
    {
        public courseworkContext()
        {
        }

        public courseworkContext(DbContextOptions<courseworkContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Director> Directors { get; set; }
        public virtual DbSet<Movie> Movies { get; set; }
        public virtual DbSet<Rating> Ratings { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=coursework;Username=postgres;Password=boogieman13");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Ukrainian_Ukraine.1251");

            modelBuilder.Entity<Director>(entity =>
            {
                entity.ToTable("director");

                entity.HasIndex(e => e.PlaceOfBirth, "i_dir_country")
                    .HasOperators(new[] { "bpchar_pattern_ops" });

                entity.HasIndex(e => e.Name, "i_dir_name")
                    .HasOperators(new[] { "bpchar_pattern_ops" });

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.IsAccessible).HasColumnName("is_accessible");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");

                entity.Property(e => e.PlaceOfBirth)
                    .IsRequired()
                    .HasColumnName("place_of_birth");
            });

            modelBuilder.Entity<Movie>(entity =>
            {
                entity.ToTable("movie");

                entity.HasIndex(e => e.Country, "i_movie_country")
                    .HasOperators(new[] { "bpchar_pattern_ops" });

                entity.HasIndex(e => e.Name, "i_movie_name")
                    .HasOperators(new[] { "bpchar_pattern_ops" });

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.Country)
                    .IsRequired()
                    .HasColumnName("country");

                entity.Property(e => e.DirectorId).HasColumnName("director_id");

                entity.Property(e => e.Duration).HasColumnName("duration");

                entity.Property(e => e.Genre)
                    .IsRequired()
                    .HasColumnName("genre");

                entity.Property(e => e.IsAccessible).HasColumnName("is_accessible");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");

                entity.Property(e => e.Year).HasColumnName("year");

                entity.HasOne(d => d.Director)
                    .WithMany(p => p.Movies)
                    .HasForeignKey(d => d.DirectorId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_dir");
            });

            modelBuilder.Entity<Rating>(entity =>
            {
                entity.ToTable("rating");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.AvgNum).HasColumnName("avg_num");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.Property(e => e.NumOfRates).HasColumnName("num_of_rates");

                entity.HasOne(d => d.Movie)
                    .WithMany(p => p.Ratings)
                    .HasForeignKey(d => d.MovieId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("fk_mov");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
