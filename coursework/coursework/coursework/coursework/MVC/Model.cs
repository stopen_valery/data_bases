﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using System.Diagnostics;
using System.IO;
using System.Xml.Linq;
using System.Text.Json;

namespace coursework
{
    class Model
    {
        private courseworkContext db;
        public Model()
        {
            db = new courseworkContext();
        }

        #region movies
        public List<Movie> movie_print(int num)
        {
            var ret = db.Movies.Take(num).ToList();
            return ret;
        }
        public Movie movie_id(int id)
        {
            var ret = db.Movies.FirstOrDefault(m => m.Id == id);
            return ret;
        }
        public string movie_get_by_id(int movie_id)
        {
            var ret = db.Movies.Where(m => m.Id == movie_id).Include(d => d.Director);
            string movie = "";
            if(ret == null)
            {
                return movie;
            }
            foreach (var r in ret)
            {
                movie += "Name: ";
                movie += r.Name;
                movie += "\nYear: ";
                movie += r.Year.ToString();
                movie += "\nDuration: ";
                movie += r.Duration.ToString();
                movie += "\nGenre: ";
                movie += r.Genre;
                movie += "\nCountry: ";
                movie += r.Genre;
                movie += "\nDirector: ";
                movie += r.Director.Name;
            }
            return movie;
        }
        public List<Movie> movie_find_name(string name)
        {
            char symb = '%';
            name = symb + name + symb;
            var ret = db.Movies.Where(m => EF.Functions.Like(m.Name, name)).ToList();
            return ret;
        }
        public List<Movie> movie_find_country(string country)
        {
            char symb = '%';
            country = symb + country + symb;
            var ret = db.Movies.Where(m => EF.Functions.Like(m.Country, country)).ToList();
            return ret;
        }
        public string movie_sort_rate(int num)
        {
            var ret = db.Ratings.Include(m => m.Movie).OrderByDescending(r => r.AvgNum).Take(num);
            string movie = "";
            if (ret == null)
            {
                return movie;
            }
            foreach (var r in ret)
            {
                movie += "Rating: ";
                movie += r.AvgNum.ToString();
                movie += " Name: ";
                movie += r.Movie.Name;
                movie += "\n";
            }
            return movie;
        }
        public string movie_sort_popularity(int num)
        {
            var ret = db.Ratings.Include(m => m.Movie).OrderByDescending(r => r.NumOfRates).Take(num);
            string movie = "";
            if (ret == null)
            {
                return movie;
            }
            foreach (var r in ret)
            {
                movie += "Number of rates: ";
                movie += r.NumOfRates.ToString();
                movie += " Name: ";
                movie += r.Movie.Name;
                movie += "\n";
            }
            return movie;
        }
        public List<Movie> movie_sort_year(int num)
        {
            var ret = db.Movies.OrderBy(m => m.Year).Take(num).ToList();
            return ret;
        }
        public int movie_add(Movie movie)
        {
            movie.IsAccessible = true;
            db.Movies.Add(movie);
            db.SaveChanges();
            Rating r = new Rating();
            r.AvgNum = 0;
            r.NumOfRates = 0;
            var m = db.Movies.Max(m => m.Id);
            r.MovieId = m;
            db.Ratings.Add(r);
            db.SaveChanges();
            return m;
        }
        public int movie_edit(Movie movie)
        {
            movie.IsAccessible = true;
            Movie m = movie_id(movie.Id);
            if(m.IsAccessible==false)
            {
                return -1;
            }
            m = movie;
            db.SaveChanges();
            return 0;
        }
        public int movie_delete(int id)
        {
            var movie = movie_id(id);
            if (movie == null)
            {
                return 0;
            }
            if(movie.IsAccessible == false)
            {
                return -1;
            }
            db.Movies.Remove(movie);
            db.SaveChanges();
            return 1;
        }
        #endregion
        #region rating
        public Rating movie_rating(int movie_id)
        {
            var ret = db.Ratings.FirstOrDefault(r => r.MovieId == movie_id);
            return ret;
        }
        public void movie_rating_add(int movie_id, int rating)
        {
            Rating rate = movie_rating(movie_id);
            rate.AvgNum = ((rate.AvgNum * rate.NumOfRates) + rating) / (rate.NumOfRates + 1);
            rate.NumOfRates++;
            db.SaveChanges();  
        }
        #endregion
        #region directors
        public List<Director> director_print(int num)
        {
            var ret = db.Directors.Take(num).ToList();
            return ret;
        }
        public Director director_id(int id)
        {
            var ret = db.Directors.FirstOrDefault(d => d.Id == id);
            return ret;
        }
        public string director_get_by_id(int director_id)
        {
            var ret = db.Movies.Where(m => m.DirectorId == director_id).Include(d => d.Director);
            string director = "";
            if (ret == null)
            {
                return director;
            }
            foreach (var r in ret)
            {
                director += "Name: ";
                director += r.Director.Name;
                director += "\nPlace of birth: ";
                director += r.Director.PlaceOfBirth;
                director += "\nMovie name: ";
                director += r.Name;
            }
            return director;
        }
        public int director_add(Director director)
        {
            director.IsAccessible = true;
            db.Directors.Add(director);
            db.SaveChanges();
            var d = db.Directors.Max(d => d.Id);
            return d;
        }
        public int director_edit(Director director)
        {
            director.IsAccessible = true;
            var d = director_id(director.Id);
            if (d.IsAccessible == false)
            {
                return -1;
            }
            d = director;
            db.SaveChanges();
            return 0;
        }
        public int director_delete(int id)
        {
            var director = director_id(id);
            if (director == null)
            {
                return 0;
            }
            if (director.IsAccessible == false)
            {
                return -1;
            }
            db.Directors.Remove(director);
            db.SaveChanges();
            return 1;
        }
        public List<Director> director_find_name(string name)
        {
            char symb = '%';
            name = symb + name + symb;
            var ret = db.Directors.Where(m => EF.Functions.Like(m.Name, name)).ToList();
            return ret;
        }
        public List<Director> director_find_country(string country)
        {
            char symb = '%';
            country = symb + country + symb;
            var ret = db.Directors.Where(m => EF.Functions.Like(m.PlaceOfBirth, country)).ToList();
            return ret;
        }
        public List<Director> director_sort_name_a(int num)
        {
            var ret = db.Directors.OrderBy(m => m.Name).Take(num).ToList();
            return ret;
        }
        public List<Director> director_sort_name_d(int num)
        {
            var ret = db.Directors.OrderByDescending(m => m.Name).Take(num).ToList();
            return ret;
        }
        #endregion

        public void backup()
        {
            string sProcess = @"C:\windows\system32\cmd.exe";
            string command = "C:/\"Program Files\"/PostgreSQL/13/bin/pg_dump.exe --file C:/Users/user/Documents/database/coursework.sql  --host localhost --port 5432 --username postgres --password --verbose --format=c --blobs coursework\n";
            string cmd = String.Format(" /k {0}{1}{2} x86", "\"", command, "\"");
            Process.Start(sProcess, cmd);
        }
        public void restore()
        {
            string sProcess = @"C:\windows\system32\cmd.exe";
            string command = "C:/\"Program Files\"/PostgreSQL/13/bin/pg_restore.exe -d coursework -U postgres -c C:/Users/user/Documents/database/coursework.sql";
            string cmd = String.Format(" /k {0}{1}{2} x86", "\"", command, "\"");
            Process.Start(sProcess, cmd);
        }

        #region export
        public void movies_export_csv()
        {
            List<Movie> movies = db.Movies.ToList();
            var sw = new StreamWriter("C:/Users/user/Documents/database/data/movies.csv", false);
            char coma = ',';
            char enter = '\n';
            int icolcount = movies.Count;
            for (int i = 0; i < icolcount; i++)
            {
                sw.Write(movies[i].Name);
                sw.Write(coma);
                sw.Write(movies[i].Year.ToString());
                sw.Write(coma);
                if(movies[i].Genre.Contains(','))
                {
                    sw.Write('\"');
                    sw.Write(movies[i].Genre);
                    sw.Write('\"');
                }
                else
                {
                    sw.Write(movies[i].Genre);
                }
                sw.Write(coma);
                if (movies[i].Country.Contains(','))
                {
                    sw.Write('\"');
                    sw.Write(movies[i].Country);
                    sw.Write('\"');
                }
                else
                {
                    sw.Write(movies[i].Country);
                }
                sw.Write(coma);
                sw.Write(movies[i].Duration.ToString());
                sw.Write(coma);
                sw.Write(movies[i].IsAccessible.ToString());
                sw.Write(coma);
                sw.Write(movies[i].DirectorId.ToString());
                sw.Write(enter);
            }

        }
        public void movies_export_xml()
        {
            List<Movie> movies = db.Movies.ToList();
            XDocument xdoc = new XDocument();
            XElement x_movies = new XElement("movies");
            int icolcount = movies.Count;
            for (int i = 0; i < icolcount; i++)
            {
                XElement movie = new XElement("movie");
                XAttribute movie_name = new XAttribute("name", movies[i].Name);
                XElement movie_country = new XElement("country", movies[i].Country);
                XElement movie_genre = new XElement("genre", movies[i].Genre);
                XElement movie_dur = new XElement("duration", movies[i].Duration.ToString());
                XElement movie_year = new XElement("year", movies[i].Year.ToString());
                XElement movie_access = new XElement("is_accessible", movies[i].IsAccessible.ToString());
                XElement movie_dir = new XElement("director_id", movies[i].DirectorId.ToString());
                movie.Add(movie_name);
                movie.Add(movie_country);
                movie.Add(movie_genre);
                movie.Add(movie_dur);
                movie.Add(movie_year);
                movie.Add(movie_access);
                movie.Add(movie_dir);
                x_movies.Add(movie);
            }

            xdoc.Add(x_movies);
            xdoc.Save("C:/Users/user/Documents/database/data/movies.xml");
        }
        public async System.Threading.Tasks.Task movies_export_json()
        {
            List<Movie> movies = db.Movies.ToList();
            using (FileStream fs = new FileStream("C:/Users/user/Documents/database/data/movies.json", FileMode.OpenOrCreate))
            {
                int icolcount = movies.Count;
                for (int i = 0; i < icolcount; i++)
                {
                    await JsonSerializer.SerializeAsync<Movie>(fs, movies[i]);
                }
            }
        }
        public void directors_export_csv()
        {
            List<Director> directors = db.Directors.ToList();
            var sw = new StreamWriter("C:/Users/user/Documents/database/data/directors.csv", false);
            char coma = ',';
            char enter = '\n';
            int icolcount = directors.Count;
            for (int i = 0; i < icolcount; i++)
            {
                sw.Write(directors[i].Name);
                sw.Write(coma);
                if (directors[i].PlaceOfBirth.Contains(','))
                {
                    sw.Write('\"');
                    sw.Write(directors[i].PlaceOfBirth);
                    sw.Write('\"');
                }
                else
                {
                    sw.Write(directors[i].PlaceOfBirth);
                }
                sw.Write(coma);
                sw.Write(directors[i].IsAccessible.ToString());
                sw.Write(enter);
            }
        }
        public void directors_export_xml()
        {
            List<Director> directors = db.Directors.ToList();
            XDocument xdoc = new XDocument();
            XElement x_directors = new XElement("directors");
            int icolcount = directors.Count;
            for (int i = 0; i < icolcount; i++)
            {
                XElement director = new XElement("director");
                XAttribute director_name = new XAttribute("name", directors[i].Name);
                XElement director_country = new XElement("place_of_bith", directors[i].PlaceOfBirth);
                XElement director_access = new XElement("is_accessible", directors[i].IsAccessible.ToString());
                director.Add(director_name);
                director.Add(director_country);
                director.Add(director_access);
                x_directors.Add(director);
            }

            xdoc.Add(x_directors);
            xdoc.Save("C:/Users/user/Documents/database/data/directors.xml");
        }
        public async System.Threading.Tasks.Task directors_export_json()
        {
            List<Director> directors = db.Directors.ToList();
            using (FileStream fs = new FileStream("C:/Users/user/Documents/database/data/directors.json", FileMode.OpenOrCreate))
            {
                int icolcount = directors.Count;
                for (int i = 0; i < icolcount; i++)
                {
                    await JsonSerializer.SerializeAsync<Director>(fs, directors[i]);
                }
            }
        }
        #endregion
    }
}
