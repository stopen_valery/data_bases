﻿using System;
using System.Collections.Generic;
using System.Text;

namespace coursework
{
    class View
    {
        public string entity()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Choose entity to work with:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.Movie\n2.Director\n3.Exit");
            return Console.ReadLine();

        }
        #region movies
        public string movie()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\nMovie Menu:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.Show n movies\n2.Show movie by ID\n3.Work with users` movies\n4.Find movie\n5.Sort movies\n6.Data analysis\n7.Export\n8.Choose another entity");
            return Console.ReadLine();
        }
        public string movie_user()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\nUsers` Movie Menu:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.Add movie\n2.Edit movie\n3.Delete movie\n4.Go back");
            return Console.ReadLine();
        }
        public string movie_find()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\nFind movies by:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.Name\n2.Country");
            return Console.ReadLine();
        }
        public string movie_sort()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\nSort movies by:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.Rating\n2.Popularity\n3.Year\n4.Go back");
            return Console.ReadLine();
        }
        public string movie_analysis()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\nAnalyze movies by:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.County\n2.Year");
            return Console.ReadLine();
        }
        public string movie_export()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\nExport into:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.CSV\n2.XML\n3.JSON");
            return Console.ReadLine();
        }
        public string movies_get_num()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Enter the number of movies to show (more than 0):");
            Console.ForegroundColor = ConsoleColor.Gray;
            string n = Console.ReadLine();
            return n;
        }
        public string movies_get_id()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Enter the ID of the movie:");
            Console.ForegroundColor = ConsoleColor.Gray;
            string n = Console.ReadLine();
            return n;
        }
        public void movies_print(List<Movie> movies)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("A list of movies:");
            Console.ForegroundColor = ConsoleColor.Gray;
            foreach (Movie m in movies)
            {
                Console.WriteLine(m.Id.ToString() + ". Name: " + m.Name +" Year: "+m.Year.ToString() + " Country: " + m.Country + " Duration: " + m.Duration.ToString() + " Genre: " + m.Genre + " Director ID: " + m.DirectorId.ToString());
            }
        }
        public string movie_get_name()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Movie name:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string movie_get_genre()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Movie genre:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string movie_get_country()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Movie country:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string movie_get_duration()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Movie duration:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string movie_get_year()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Movie year:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string movie_get_dirid()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Movie director ID:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        #endregion
        #region rating
        public string movies_rating_question()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Do you want to know about movie rating? (y/n)");
            Console.ForegroundColor = ConsoleColor.Gray;
            string key = Console.ReadKey().KeyChar.ToString();
            return key;
        }
        public string rating_print(Rating rating)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\nRating of movie with id " + rating.MovieId+ ": ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("Rating: " + rating.AvgNum + "\nNumber of rates: " + rating.NumOfRates);
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Do you want to rate this movie? (y/n)");
            Console.ForegroundColor = ConsoleColor.Gray;
            string key = Console.ReadKey().KeyChar.ToString();
            return key;
        }
        public string rating_get()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\nInput your rate: (from 0 to 10)");
            Console.ForegroundColor = ConsoleColor.Gray;
            string inp = Console.ReadLine();
            return inp;
        }
        public void rating_new_print(Rating rating)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Rate added successfully\nNow movie rating is:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("Rating: " + rating.AvgNum + "\nNumber of rates: " + rating.NumOfRates);
        }
        #endregion
        public void print_string(string str, string entity)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(entity+":");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(str);
        }
        #region directors
        public string director()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\nDirector Menu:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.Show n directors\n2.Show director by ID\n3.Work with users` directors\n4.Find directors\n5.Sort directors\n6.Export\n7.Choose another entity");
            return Console.ReadLine();
        }
        public string director_user()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\nUsers` Director Menu:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.Add director\n2.Edit director\n3.Delete director\n4.Go back");
            return Console.ReadLine();
        }
        public string directors_export()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\nExport into:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.CSV\n2.XML\n3.JSON");
            return Console.ReadLine();
        }
        public string directors_get_num()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Enter the number of directors to show (more than 0):");
            Console.ForegroundColor = ConsoleColor.Gray;
            string n = Console.ReadLine();
            return n;
        }
        public string directors_get_id()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Enter the ID of the director:");
            Console.ForegroundColor = ConsoleColor.Gray;
            string n = Console.ReadLine();
            return n;
        }
        public void directors_print(List<Director> directors)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("A list of directors:");
            Console.ForegroundColor = ConsoleColor.Gray;
            foreach (Director d in directors)
            {
                Console.WriteLine(d.Id.ToString() + ". Name: " + d.Name + " Place of birth: " + d.PlaceOfBirth);
            }
        }
        public string director_get_name()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Director name:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string director_get_country()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Director county of birth:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string director_find()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\nFind directors by:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.Name\n2.Country of birth\n3.Go back");
            return Console.ReadLine();
        }
        public string director_sort()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\nSort movies by name:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.Ascending order \n2.Descending order\n3.Go back");
            return Console.ReadLine();
        }
        #endregion

        #region ERRORS
        public void error_option()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("ERROR.Wrong oprtion");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public void error_string()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("ERROR.You`ve entered a string");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public void error_empty_string()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("ERROR.The field cannot be empty");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public void error_out_of_range()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("ERROR.The number is out of range");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public void error_empty_list()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("ERROR.No entity with such parameters");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public void error_access()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("ERROR.You have no rights to change this entity");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        #endregion
        public void successfull_operation(string entity, int ID, string operation)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(entity + " with ID " + ID + " " + operation + "  successfully");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}
