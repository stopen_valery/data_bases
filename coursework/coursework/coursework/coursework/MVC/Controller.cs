﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace coursework
{
    class Controller
    {
        Model model = new Model();
        View view = new View();

        public int entity_menu()
        {
            while (1 == 1)
            {
                int entity = 0;
                Int32.TryParse(view.entity(), out entity);
                if (entity == 1)
                {
                    movie_menu();
                    break;
                }
                else if (entity == 2)
                {
                    director_menu();
                    break;
                }
                else if (entity == 4)
                {
                    model.backup();
                    break;
                }
                else if (entity == 3)
                {
                    break;
                }
                else
                {
                    view.error_option();
                }
            }
            return 1;
        }
        #region movies
        private void movie_menu()
        {
            while (1 == 1)
            {
                int movie = 0;
                Int32.TryParse(view.movie(), out movie);
                if (movie == 1)
                {
                    string s_num = view.movies_get_num();
                    int num = 0;
                    if (!Int32.TryParse(s_num, out num))
                    {
                        view.error_string();
                    }
                    else if (num <= 0)
                    {
                        view.error_out_of_range();
                    }
                    else
                    {
                        List<Movie> movies = model.movie_print(num);
                        view.movies_print(movies);
                    }
                }
                else if (movie == 2)
                {
                    string s_id = view.movies_get_id();
                    int id = 0;
                    if (!Int32.TryParse(s_id, out id))
                    {
                        view.error_string();
                    }
                    else
                    {
                        string movies = model.movie_get_by_id(id);
                        if (movies.Length == 0)
                        {
                            view.error_empty_list();
                        }
                        else
                        {
                            view.print_string(movies, "Movie");
                            string option = view.movies_rating_question();
                            if (option == "y")
                            {
                                Rating rating = model.movie_rating(id);
                                option = view.rating_print(rating);
                                if (option == "y")
                                {
                                    int rate = 0;
                                    while (!Int32.TryParse(view.rating_get(), out rate) || rate < 0 || rate > 10)
                                    {
                                        view.error_out_of_range();
                                    }
                                    model.movie_rating_add(id, rate);
                                    rating = model.movie_rating(id);
                                    view.rating_new_print(rating);
                                }
                            }
                        }
                    }
                }
                else if (movie == 3)
                {
                    user_movie_menu();
                    break;
                }
                else if (movie == 4)
                {
                    find_movie_menu();
                    break;
                }
                else if (movie == 5)
                {
                    sort_movie_menu();
                    break;
                }
                else if (movie == 6)
                {
                    analysis_movie_menu();
                    break;
                }
                else if (movie == 7)
                {
                    movie_export();
                    break;
                }
                else if (movie == 8)
                {
                    break;
                }
                else
                {
                    view.error_option();
                }
            }
        }
        private void user_movie_menu()
        {
            while (1 == 1)
            {
                int movie = 0;
                Int32.TryParse(view.movie_user(), out movie);
                if (movie == 1)
                {
                    Movie m = new Movie();
                    m.Name = view.movie_get_name();
                    while (m.Name.Length == 0)
                    {
                        view.error_empty_string();
                        m.Name = view.movie_get_name();
                    }
                    m.Genre = view.movie_get_genre();
                    while (m.Genre.Length == 0)
                    {
                        view.error_empty_string();
                        m.Genre = view.movie_get_genre();
                    }
                    m.Country = view.movie_get_country();
                    while (m.Country.Length == 0)
                    {
                        view.error_empty_string();
                        m.Country = view.movie_get_country();
                    }
                    int duration = 0;
                    string s_duration = view.movie_get_duration();
                    while (!Int32.TryParse(s_duration, out duration))
                    {
                        view.error_string();
                        s_duration = view.movie_get_duration();
                    }
                    m.Duration = duration;
                    int year = 0;
                    string s_year = view.movie_get_year();
                    while (!Int32.TryParse(s_year, out year))
                    {
                        view.error_string();
                        s_duration = view.movie_get_year();
                    }
                    m.Year = year;
                    int dir_id = 0;
                    string s_dir_id = view.movie_get_dirid();
                    Int32.TryParse(s_dir_id, out dir_id);
                    while (model.director_id(dir_id) == null)
                    {
                        view.error_empty_list();
                        s_dir_id = view.movie_get_dirid();
                        Int32.TryParse(s_dir_id, out dir_id);
                    }
                    m.DirectorId = dir_id;
                    int new_id = model.movie_add(m);
                    view.successfull_operation("Movie", new_id, "added");
                }
                else if (movie == 2)
                {
                    int id = 0;
                    Int32.TryParse(view.movies_get_id(), out id);
                    if (model.movie_id(id) == null)
                    {
                        view.error_empty_list();
                    }
                    else
                    {
                        Movie m = new Movie();
                        m.Name = view.movie_get_name();
                        while (m.Name.Length == 0)
                        {
                            view.error_empty_string();
                            m.Name = view.movie_get_name();
                        }
                        m.Genre = view.movie_get_genre();
                        while (m.Genre.Length == 0)
                        {
                            view.error_empty_string();
                            m.Genre = view.movie_get_genre();
                        }
                        m.Country = view.movie_get_country();
                        while (m.Country.Length == 0)
                        {
                            view.error_empty_string();
                            m.Country = view.movie_get_country();
                        }
                        int duration = 0;
                        string s_duration = view.movie_get_duration();
                        while (!Int32.TryParse(s_duration, out duration))
                        {
                            view.error_string();
                            s_duration = view.movie_get_duration();
                        }
                        m.Duration = duration;
                        int year = 0;
                        string s_year = view.movie_get_year();
                        while (!Int32.TryParse(s_year, out year))
                        {
                            view.error_string();
                            s_duration = view.movie_get_year();
                        }
                        m.Year = year;
                        int dir_id = 0;
                        string s_dir_id = view.movie_get_dirid();
                        Int32.TryParse(s_dir_id, out dir_id);
                        while (model.director_id(dir_id) == null)
                        {
                            view.error_empty_list();
                            s_dir_id = view.movie_get_dirid();
                            Int32.TryParse(s_dir_id, out dir_id);
                        }
                        m.DirectorId = dir_id;
                        m.Id = id;
                        int res = model.movie_edit(m);
                        if (res == -1)
                        {
                            view.error_access();
                        }
                        else
                        {
                            view.successfull_operation("Movie", id, "edited");
                        }
                    }
                }
                else if (movie == 3)
                {
                    int id = 0;
                    Int32.TryParse(view.movies_get_id(), out id);
                    int del = model.movie_delete(id);
                    if (del == 0)
                    {
                        view.error_empty_list();
                    }
                    else if (del == -1)
                    {
                        view.error_access();
                    }
                    else
                    {
                        view.successfull_operation("Movie", id, "deleted");
                    }
                }
                else if (movie == 4)
                {
                    break;
                }
                else
                {
                    view.error_option();
                }
            }
        }
        private void find_movie_menu()
        {
            while (1 == 1)
            {
                int movie = 0;
                Int32.TryParse(view.movie_find(), out movie);
                if (movie == 1)
                {
                    string name = view.movie_get_name();
                    while (name.Length == 0)
                    {
                        view.error_empty_string();
                        name = view.movie_get_name();
                    }
                    List<Movie> movies = model.movie_find_name(name);
                    if (movies.Count == 0)
                    {
                        view.error_empty_list();
                    }
                    else
                    {
                        view.movies_print(movies);
                    }
                }
                else if(movie == 2)
                {
                    string country = view.movie_get_country();
                    while (country.Length == 0)
                    {
                        view.error_empty_string();
                        country = view.movie_get_country();
                    }
                    List<Movie> movies = model.movie_find_country(country);
                    if (movies.Count == 0)
                    {
                        view.error_empty_list();
                    }
                    else
                    {
                        view.movies_print(movies);
                    }
                }
                else if (movie == 3)
                {
                    break;
                }
                else
                {
                    view.error_option();
                }
            }
        }
        private void sort_movie_menu()
        {
            while (1 == 1)
            {
                int movie = 0;
                Int32.TryParse(view.movie_sort(), out movie);
                if (movie == 1)
                {
                    string s_num = view.movies_get_num();
                    int num = 0;
                    if (!Int32.TryParse(s_num, out num))
                    {
                        view.error_string();
                    }
                    else if (num <= 0)
                    {
                        view.error_out_of_range();
                    }
                    else
                    {
                        string movies = model.movie_sort_rate(num);
                        view.print_string(movies, "Movies");
                    }
                }
                else if(movie == 2)
                {
                    string s_num = view.movies_get_num();
                    int num = 0;
                    if (!Int32.TryParse(s_num, out num))
                    {
                        view.error_string();
                    }
                    else if (num <= 0)
                    {
                        view.error_out_of_range();
                    }
                    else
                    {
                        string movies = model.movie_sort_popularity(num);
                        view.print_string(movies, "Movies");
                    }
                }
                else if (movie == 3)
                {
                    string s_num = view.movies_get_num();
                    int num = 0;
                    if (!Int32.TryParse(s_num, out num))
                    {
                        view.error_string();
                    }
                    else if (num <= 0)
                    {
                        view.error_out_of_range();
                    }
                    else
                    {
                        List <Movie>  movies = model.movie_sort_year(num);
                        view.movies_print(movies);
                    }
                }
                else if (movie == 4)
                {
                    break;
                }
                else
                {
                    view.error_option();
                }
            }
        }
        private void analysis_movie_menu()
        {
            while (1 == 1)
            {
                int movie = 0;
                Int32.TryParse(view.movie_analysis(), out movie);
                if (movie == 1)
                {
                    var proc = Process.Start(@"cmd.exe ", @"/c C:/Users/user/Desktop/bd/coursework/coursework/country_analysis/country.html");
                }
                else if(movie == 2)
                {
                    var proc = Process.Start(@"cmd.exe ", @"/c C:/Users/user/Desktop/bd/coursework/coursework/year_analysis/year.html");
                }
                else if(movie == 3)
                {
                    break;
                }
                else
                {
                    view.error_option();
                }
            }
        }
        private void movie_export()
        {
            while (1 == 1)
            {
                int movie = 0;
                Int32.TryParse(view.movie_export(), out movie);
                if (movie == 1)
                {
                    model.movies_export_csv();
                }
                else if(movie == 2)
                {
                    model.movies_export_xml();
                }
                else if (movie == 3)
                {
                    model.movies_export_json();
                }
                else if (movie == 4)
                {
                    break;
                }
                else
                {
                    view.error_option();
                }
            }
        }

    #endregion
    #region directors
                private void director_menu()
        {
            while (1 == 1)
            {
                int director = 0;
                Int32.TryParse(view.director(), out director);
                if (director == 1)
                {
                    string s_num = view.directors_get_num();
                    int num = 0;
                    if (!Int32.TryParse(s_num, out num))
                    {
                        view.error_string();
                    }
                    else if (num <= 0)
                    {
                        view.error_out_of_range();
                    }
                    else
                    {
                        List<Director> directors = model.director_print(num);
                        view.directors_print(directors);
                    }
                }
                else if (director == 2)
                {
                    string s_id = view.directors_get_id();
                    int id = 0;
                    if (!Int32.TryParse(s_id, out id))
                    {
                        view.error_string();
                    }
                    else
                    {
                        string directors = model.director_get_by_id(id);
                        if (directors.Length == 0)
                        {
                            view.error_empty_list();
                        }
                        else
                        {
                            view.print_string(directors, "Director");
                        }
                    }
                }
                else if (director == 3)
                {
                    user_director_menu();
                    break;
                }
                else if (director == 4)
                {
                    find_director_menu();
                }
                else if (director == 5)
                {
                    sort_director_menu();
                }
                else if (director == 6)
                {
                    director_export();
                }
                else if (director == 7)
                {
                    break;
                }
                else
                {
                    view.error_option();
                }
            }

        }
        private void user_director_menu()
        {
            while (1 == 1)
            {
                int director = 0;
                Int32.TryParse(view.director_user(), out director);
                if (director == 1)
                {
                    Director d = new Director();
                    d.Name = view.director_get_name();
                    while (d.Name.Length == 0)
                    {
                        view.error_empty_string();
                        d.Name = view.director_get_name();
                    }
                    d.PlaceOfBirth = view.director_get_country();
                    while (d.PlaceOfBirth.Length == 0)
                    {
                        view.error_empty_string();
                        d.PlaceOfBirth = view.director_get_country();
                    }
                    int new_id = model.director_add(d);
                    view.successfull_operation("Director", new_id, "added");
                }
                else if (director == 2)
                {
                    int id = 0;
                    Int32.TryParse(view.directors_get_id(), out id);
                    if (model.director_get_by_id(id) == null)
                    {
                        view.error_empty_list();
                    }
                    else
                    {
                        Director d = new Director();
                        d.Name = view.director_get_name();
                        while (d.Name.Length == 0)
                        {
                            view.error_empty_string();
                            d.Name = view.director_get_name();
                        }
                        d.PlaceOfBirth = view.director_get_country();
                        while (d.PlaceOfBirth.Length == 0)
                        {
                            view.error_empty_string();
                            d.PlaceOfBirth = view.director_get_country();
                        }
                        d.Id = id;
                        int res = model.director_edit(d);
                        if (res == -1)
                        {
                            view.error_access();
                        }
                        else
                        {
                            view.successfull_operation("Director", id, "edited");
                        }
                    }
                }
                else if (director == 3)
                {
                    int id = 0;
                    Int32.TryParse(view.directors_get_id(), out id);
                    int del = model.director_delete(id);
                    if (del == 0)
                    {
                        view.error_empty_list();
                    }
                    else if (del == -1)
                    {
                        view.error_access();
                    }
                    else
                    {
                        view.successfull_operation("Director", id, "deleted");
                    }
                }
                else if (director == 4)
                {
                    break;
                }
                else
                {
                    view.error_option();
                }
            }
        }
        private void sort_director_menu()
        {
            while (1 == 1)
            {
                int director = 0;
                Int32.TryParse(view.director_sort(), out director);
                if (director == 1)
                {
                    string s_num = view.directors_get_num();
                    int num = 0;
                    if (!Int32.TryParse(s_num, out num))
                    {
                        view.error_string();
                    }
                    else if (num <= 0)
                    {
                        view.error_out_of_range();
                    }
                    else
                    {
                        List<Director> directors = model.director_sort_name_a(num);
                        view.directors_print(directors);
                    }
                }
                else if (director == 2)
                {
                    string s_num = view.directors_get_num();
                    int num = 0;
                    if (!Int32.TryParse(s_num, out num))
                    {
                        view.error_string();
                    }
                    else if (num <= 0)
                    {
                        view.error_out_of_range();
                    }
                    else
                    {
                        List<Director> directors = model.director_sort_name_d(num);
                        view.directors_print(directors);
                    }
                }
                else if (director == 3)
                {
                    break;
                }
                else
                {

                }
            }
        }
        private void find_director_menu()
        {
            while (1 == 1)
            {
                int director = 0;
                Int32.TryParse(view.director_find(), out director);
                if (director == 1)
                {
                    string name = view.director_get_name();
                    while (name.Length == 0)
                    {
                        view.error_empty_string();
                        name = view.director_get_name();
                    }
                    List<Director> directors = model.director_find_name(name);
                    if (directors.Count == 0)
                    {
                        view.error_empty_list();
                    }
                    else
                    {
                        view.directors_print(directors);
                    }
                }
                else if (director == 2)
                {
                    string country = view.director_get_country();
                    while (country.Length == 0)
                    {
                        view.error_empty_string();
                        country = view.director_get_country();
                    }
                    List<Director> directors = model.director_find_country(country);
                    if (directors.Count == 0)
                    {
                        view.error_empty_list();
                    }
                    else
                    {
                        view.directors_print(directors);
                    }
                }
                else if (director == 3)
                {
                    break;
                }
                else
                {
                    view.error_option();
                }
            }
        }
        private void director_export()
        {
            while (1 == 1)
            {
                int director = 0;
                Int32.TryParse(view.directors_export(), out director);
                if (director == 1)
                {
                    model.directors_export_csv();
                }
                else if (director == 2)
                {
                    model.directors_export_xml();
                }
                else if (director == 3)
                {
                    model.directors_export_json();
                }
                else if (director == 4)
                {
                    break;
                }
                else
                {
                    view.error_option();
                }
            }
        }
        #endregion


    }

}

