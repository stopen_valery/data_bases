﻿using System;

namespace lab2.MVC
{
    class View
    {
        public string entity ()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Menu:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.Movie\n2.Director\n3.Award\n4.Movies - Awards\n5.Search operations\n6.Exit");
            return Console.ReadLine();
        }
        #region movie
        public string movie()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Movie Menu:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.Print list of movies\n2.Print movie by ID\n3.Add movie\n4.Delete movie by ID\n5.Edit movie by ID\n6.Random Generation of movies\n7.Choose another entity");
            return Console.ReadLine();
        }
        public string movie_get_name()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Movie name:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string movie_get_genre()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Movie genre:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string movie_get_country()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Movie country:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string movie_get_duration()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Movie duration:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string movie_get_dirid()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Movie director ID:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        #endregion
        #region director
        public string director()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Director Menu:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.Print list of directors\n2.Print director by ID\n3.Add director\n4.Delete director by ID\n5.Edit director by ID\n6.Random Generation of directors\n7.Choose another entity");
            return Console.ReadLine();
        }
        public string director_get_name()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Director name:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string director_get_country()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Director county of birth:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        #endregion
        #region award
        public string award()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Award Menu:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.Print list of awards\n2.Print award by ID\n3.Add award\n4.Delete award by ID\n5.Edit award by ID\n6.Random Generation of awards\n7.Choose another entity");
            string a = Console.ReadLine();
            return a;
        }
        public string award_get_category()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Award category:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }

        public string award_get_year()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Award year:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        #endregion
        #region movie_award
        public string movie_award()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Movies-Awards Menu:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.Print connections\n2.Add connection\n3.Delete connection\n4.Random Generation of connections\n5.Choose another entity");
            string a = Console.ReadLine();
            return a;
        }
        public string get_movie_id ()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Input ID of movie:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string get_award_id()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Input ID of award:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        #endregion
        #region search
        public string search()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Search Operations:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("1.Search for movies where duration and director ID are limited and connected awards year is limited\n2.Search for movies where duration is limited and name has specific substring and connected directors name has specific substring\n3.Search for movies where name has specific substring and connected directors name has specific substring and connected awards year is limited\n4.Go to entities menu");
            string a = Console.ReadLine();
            return a;
        }
        public string search_s_duration()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Input lower border of the duration interval:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string search_e_duration()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Input upper border of the duration interval:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string search_s_id()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Input lower border of the director ID interval:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string search_e_id()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Input uper border of the director ID interval:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string search_s_year()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Input lower border of the year interval:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string search_e_year()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Input upper border of the year interval:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string search_d_name()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Input substring from director`s name:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string search_m_name()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Input substring from movie`s name:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        #endregion

        public void print(string entities)
        {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("A list of entities:");
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine(entities);
        }
        public string get_id()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Input ID of entity:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
        public string get_num()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Input number of randomly generated entities:");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }


        #region errors
        public void err_wrong_entity ()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"The entity with such a number does not exist or you've entered a string");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public void err_wrong_option()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"The option with such a number does not exis or you've entered a string");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public void err_empty_table(string entity)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(entity + " table is empty");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public void err_wrong_ID (string entity)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(entity + " with ID does not exist or you've entered a string");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public void err_empty (string entity)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(entity + " cannot be empty");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public void err_number(string entity)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine( entity + " shold be a number");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public void err_generation ()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Number shold be between 0 and 100 000");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public void err_connection()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Connection does not exist");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        #endregion
        #region successfull
        public void successfull_operation (string entity, int ID, string operation)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine( entity + " with ID " + ID + " "+ operation + "  successfully");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public void successfull_generation(string entity, int num)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(num +  " " +entity+ " generated successfully");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public void successfull_connection()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("New connection added successfully");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public void successfull_connection_delete(int movie_id, int award_id)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Connection " + movie_id + " <---> " + award_id + " deleted successfully");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        #endregion
    }
}
