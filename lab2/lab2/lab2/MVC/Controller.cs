﻿using System;

namespace lab2.MVC
{
    class Controller
    {
        Model model = new Model();
        View view = new View();
        
        public int entity_menu()
        {
            while (1 == 1)
            {
                int entity = 0;
                Int32.TryParse(view.entity(), out entity);
                if(entity == 1)
                {
                    movie_menu();
                    break;
                }
                else if (entity == 2)
                {
                    director_menu();
                    break;
                }
                else if (entity == 3)
                {
                    award_menu();
                    break;
                }
                else if (entity == 4)
                {
                    movie_award_menu();
                    break;
                }
                else if (entity == 5)
                {
                    search_menu();
                    break;
                }
                else if (entity == 6)
                {
                    return 1;
                }
                else
                {
                    view.err_wrong_entity();
                } 
            }
            return 0;
        }
        #region movie
        private void movie_menu()
        {
            while (1 == 1)
            {
                int movie = 0;
                Int32.TryParse(view.movie(), out movie);
                if (movie == 1)
                {
                    string movies = model.movie_print();
                    if (movies.Length == 0)
                    {
                        view.err_empty_table("Movies");
                    }
                    else
                    {
                        view.print(movies);
                    }
                }
                else if (movie == 2)
                {
                    int id = 0;
                    Int32.TryParse(view.get_id(), out id);
                    string movies = model.movie_get_by_id(id);
                    if (movies.Length == 0)
                    {
                        view.err_wrong_ID("Movie ");
                    }
                    else
                    {
                        view.print(movies);
                    }
                }
                else if (movie == 3)
                {
                    string name = view.movie_get_name();
                    while(name.Length == 0)
                    {
                        view.err_empty("Movie name");
                        name = view.movie_get_name();
                    }
                    string genre = view.movie_get_genre();
                    while (genre.Length == 0)
                    {
                        view.err_empty("Movie genre");
                        genre = view.movie_get_genre();
                    }
                    string country = view.movie_get_country();
                    while (country.Length == 0)
                    {
                        view.err_empty("Movie country");
                        country = view.movie_get_country();
                    }
                    int duration = 0;
                    string s_duration = view.movie_get_duration();
                    while (!Int32.TryParse(s_duration, out duration))
                    {
                        view.err_number("Movie duration");
                        s_duration = view.movie_get_duration();
                    }
                    int dir_id = 0;
                    string s_dir_id = view.movie_get_dirid();
                    Int32.TryParse(s_dir_id, out dir_id);
                    while (model.director_get_by_id(dir_id).Length == 0)
                    {
                        view.err_wrong_ID("Director ");
                        s_dir_id = view.movie_get_dirid();
                        Int32.TryParse(s_dir_id, out dir_id);
                    }
                    int new_id =  model.movie_add(name, genre, country, duration, dir_id);
                    view.successfull_operation("Movie", new_id, "added");
                }
                else if (movie == 4)
                {
                    int id = 0;
                    Int32.TryParse(view.get_id(), out id);
                    if(model.movie_get_by_id(id).Length == 0)
                    {
                        view.err_wrong_ID("Movie ");
                    }
                    else
                    {
                        model.movie_delete(id);
                        view.successfull_operation("Movie", id, "deleted");
                    }
                }
                else if (movie == 5)
                {
                    int id = 0;
                    Int32.TryParse(view.get_id(), out id);
                    if (model.movie_get_by_id(id).Length == 0)
                    {
                        view.err_wrong_ID("Movie ");
                    }
                    else
                    {
                        string name = view.movie_get_name();
                        while (name.Length == 0)
                        {
                            view.err_empty("Movie name");
                            name = view.movie_get_name();
                        }
                        string genre = view.movie_get_genre();
                        while (genre.Length == 0)
                        {
                            view.err_empty("Movie genre");
                            genre = view.movie_get_genre();
                        }
                        string country = view.movie_get_country();
                        while (country.Length == 0)
                        {
                            view.err_empty("Movie country");
                            country = view.movie_get_country();
                        }
                        int duration = 0;
                        string s_duration = view.movie_get_duration();
                        while (!Int32.TryParse(s_duration, out duration))
                        {
                            view.err_number("Movie duration"); 
                            s_duration = view.movie_get_duration();
                        }
                        int dir_id = 0;
                        string s_dir_id = view.movie_get_dirid();
                        Int32.TryParse(s_dir_id, out dir_id);
                        while (model.director_get_by_id(dir_id).Length == 0)
                        {
                            view.err_wrong_ID("Director ");
                            s_dir_id = view.movie_get_dirid();
                            Int32.TryParse(s_dir_id, out dir_id);
                        }
                        model.movie_edit(name, genre, country, duration, dir_id, id);
                        view.successfull_operation("Movie", id, "edited");
                    }
                }
                else if (movie == 6)
                {
                    int num = 0;
                    while (!Int32.TryParse(view.get_num(), out num) || num <=0 || num > 100000)
                    {
                        view.err_generation();
                    }
                    model.movie_generation(num);
                    view.successfull_generation("movies", num);
                }
                else if (movie == 7)
                {
                    break;
                }
                else
                {
                    view.err_wrong_option();
                }
            }
        }
        #endregion
        #region director
        private void director_menu()
        {
            while (1 == 1)
            {
                int director = 0;
                Int32.TryParse(view.director(), out director);
                if(director == 1)
                {
                    string directors = model.director_print();
                    if (directors.Length == 0)
                    {
                        view.err_empty_table("Director");
                    }
                    else
                    {
                        view.print(directors);
                    }
                }
                else if(director == 2)
                {
                    int id = 0;
                    Int32.TryParse(view.get_id(), out id);
                    string directors = model.director_get_by_id(id);
                    if (directors.Length == 0)
                    {
                        view.err_wrong_ID("Director");
                    }
                    else
                    {
                        view.print(directors);
                    }
                }
                else if(director == 3)
                {
                    string name = view.director_get_name();
                    while (name.Length == 0)
                    {
                        view.err_empty("Director name");
                        name = view.director_get_name();
                    }
                    string country = view.director_get_country();
                    while (country.Length == 0)
                    {
                        view.err_empty("Director country");
                        country = view.director_get_country();
                    }
                    int new_id = model.director_add(name, country);
                    view.successfull_operation("Director", new_id, "added");
                }
                else if(director == 4)
                {
                    int id = 0;
                    Int32.TryParse(view.get_id(), out id);
                    if (model.director_get_by_id(id).Length == 0)
                    {
                        view.err_wrong_ID("Director");
                    }
                    else
                    {
                        model.director_delete(id);
                        view.successfull_operation("Director", id, "deleted");
                    }
                }
                else if(director == 5)
                {
                    int id = 0;
                    Int32.TryParse(view.get_id(), out id);
                    if (model.director_get_by_id(id).Length == 0)
                    {
                        view.err_wrong_ID("Director");
                    }
                    else
                    {
                        string name = view.director_get_name();
                        while (name.Length == 0)
                        {
                            view.err_empty("Director name");
                            name = view.director_get_name();
                        }
                        string country = view.director_get_country();
                        while (country.Length == 0)
                        {
                            view.err_empty("Director country");
                            country = view.director_get_country();
                        }
                        model.director_edit(name, country, id);
                        view.successfull_operation("Director", id, "edited") ;
                    }
                }
                else if (director == 6)
                {
                    int num = 0;
                    while (!Int32.TryParse(view.get_num(), out num) || num <= 0 || num > 100000)
                    {
                        view.err_generation();
                    }
                    model.director_generation(num);
                    view.successfull_generation("directors", num);
                }
                else if (director == 7)
                {
                    break;
                }
                else
                {
                    view.err_wrong_option();

                }
            }
        }
        #endregion
        #region award
        private void award_menu()
        {
            while (1 == 1)
            {
                int award = 0;
                Int32.TryParse(view.award(), out award);
                if (award == 1)
                {
                    string awards = model.award_print();
                    if (awards.Length == 0)
                    {
                        view.err_empty_table("Awards");
                    }
                    else
                    {
                        view.print(awards);
                    }
                }
                else if (award == 2)
                {
                    int id = 0;
                    Int32.TryParse(view.get_id(), out id);
                    string awards = model.award_get_by_id(id);
                    if (awards.Length == 0)
                    {
                        view.err_wrong_ID("Award");
                    }
                    else
                    {
                        view.print(awards);
                    }
                }
                else if (award == 3)
                {
                    string category = view.award_get_category();
                    while (category.Length == 0)
                    {
                        view.err_empty("Award category");
                        category = view.award_get_category();
                    }
                    int year = 0;
                    string y = view.award_get_year();
                    while (!Int32.TryParse(y, out year))
                    {
                        view.err_number("Award year");
                        y = view.award_get_year();
                    }
                    int new_id = model.award_add(category, year);
                    view.successfull_operation("Award", new_id, "added");
                }
                else if (award == 4)
                {
                    int id = 0;
                    Int32.TryParse(view.get_id(), out id);
                    if (model.award_get_by_id(id).Length == 0)
                    {
                        view.err_wrong_ID("Award");
                    }
                    else
                    {
                        model.award_delete(id);
                        view.successfull_operation("Award", id, "deleted");
                    }
                }
                else if (award == 5)
                {
                    int id = 0;
                    Int32.TryParse(view.get_id(), out id);
                    if (model.award_get_by_id(id).Length == 0)
                    {
                        view.err_wrong_ID("Award");
                    }
                    else
                    {
                        string category = view.award_get_category();
                        while (category.Length == 0)
                        {
                            view.err_empty("Award category");
                            category = view.award_get_category();
                        }
                        int year = 0;
                        string y = view.award_get_year();
                        while (!Int32.TryParse(y, out year))
                        {
                            view.err_number("Award year");
                            y = view.award_get_year();
                        }
                        model.award_edit(category, year, id);
                        view.successfull_operation("Award", id, "edited");
                    }
                }
                else if (award == 6)
                {
                    int num = 0;
                    while (!Int32.TryParse(view.get_num(), out num) || num <= 0 || num > 100000)
                    {
                        view.err_generation();
                    }
                    model.award_generation(num);
                    view.successfull_generation("awards", num);
                }
                else if (award == 7)
                {
                    break;
                }
                else
                {
                    view.err_wrong_option();
                }
            }
        }
        #endregion
        #region movie_award
        private void movie_award_menu()
        {
            while (1 == 1)
            {
                int movie_award = 0;
                Int32.TryParse(view.movie_award(), out movie_award);
                if (movie_award == 1)
                {
                    string movies_awards = model.movie_award_print();
                    if (movies_awards.Length == 0)
                    {
                        view.err_empty_table("Movie - Awards");
                    }
                    else
                    {
                        view.print(movies_awards);
                    }
                }
                else if (movie_award == 2)
                {
                    int movie_id = 0;
                    Int32.TryParse(view.get_movie_id(), out movie_id);
                    while (model.movie_get_by_id(movie_id).Length == 0)
                    {
                        view.err_wrong_ID("Movie");
                        Int32.TryParse(view.get_movie_id(), out movie_id);
                    }
                    int award_id = 0;
                    Int32.TryParse(view.get_award_id(), out award_id);
                    while (model.award_get_by_id(award_id).Length == 0)
                    {
                        view.err_wrong_ID("Award");
                        Int32.TryParse(view.get_award_id(), out award_id);
                    }
                    model.movie_award_add(movie_id, award_id);
                    view.successfull_connection();
                }
                else if (movie_award == 3)
                {
                    int movie_id = 0;
                    Int32.TryParse(view.get_movie_id(), out movie_id);
                    while (model.movie_get_by_id(movie_id).Length == 0)
                    {
                        view.err_wrong_ID("Movie");
                        Int32.TryParse(view.get_movie_id(), out movie_id);
                    }
                    int award_id = 0;
                    Int32.TryParse(view.get_award_id(), out award_id);
                    while (model.award_get_by_id(award_id).Length == 0)
                    {
                        view.err_wrong_ID("Award");
                        Int32.TryParse(view.get_award_id(), out award_id);
                    }
                    string del = model.movie_award_delete(movie_id, award_id);
                    if(del == "0")
                    {
                        view.err_connection();
                    }
                    else
                    {
                        view.successfull_connection_delete(movie_id, award_id);
                    }
                }
                else if(movie_award == 4)
                {
                    int num = 0;
                    while (!Int32.TryParse(view.get_num(), out num) || num <= 0 || num > 100000)
                    {
                        view.err_generation();
                    }
                    model.movie_award_generation(num);
                    view.successfull_generation("connections", num);
                }
                else if (movie_award == 5)
                {
                    break;
                }
                else
                {
                    view.err_wrong_option();
                }
            }
        }
        #endregion
        #region search
        private void search_menu()
        {
            while (1 == 1)
            {
                int search = 0;
                Int32.TryParse(view.search(), out search);
                if (search == 1)
                {
                    int s_duration = 0;
                    while(!Int32.TryParse(view.search_s_duration(), out s_duration))
                    {
                        view.err_number("Input");
                    }
                    int e_duration = 0;
                    while (!Int32.TryParse(view.search_e_duration(), out e_duration))
                    {
                        view.err_number("Input");
                    }
                    int s_id = 0;
                    while (!Int32.TryParse(view.search_s_id(), out s_id))
                    {
                        view.err_number("Input");
                    }
                    int e_id = 0;
                    while (!Int32.TryParse(view.search_e_id(), out e_id))
                    {
                        view.err_number("Input");
                    }
                    int s_year = 0;
                    while (!Int32.TryParse(view.search_s_year(), out s_year))
                    {
                        view.err_number("Input");
                    }
                    int e_year = 0;
                    while (!Int32.TryParse(view.search_e_year(), out e_year))
                    {
                        view.err_number("Input");
                    }
                    string searches = model.search_1(s_duration, e_duration, s_id, e_id, s_year, e_year);
                    if (searches.Length == 0)
                    {
                        view.err_empty_table("This");
                    }
                    else
                    {
                        view.print(searches);
                    }
                }
                else if (search == 2)
                {
                    int s_duration = 0;
                    while (!Int32.TryParse(view.search_s_duration(), out s_duration))
                    {
                        view.err_number("Input");
                    }
                    int e_duration = 0;
                    while (!Int32.TryParse(view.search_e_duration(), out e_duration))
                    {
                        view.err_number("Input");
                    }
                    string d_name = view.search_d_name();
                    while(d_name.Length == 0)
                    {
                        view.err_empty("Substring");
                        d_name = view.search_d_name();
                    }
                    string m_name = view.search_m_name();
                    while (m_name.Length == 0)
                    {
                        view.err_empty("Substring");
                        m_name = view.search_m_name();
                    }
                    string searches = model.search_2(d_name, e_duration, s_duration, m_name);
                    if (searches.Length == 0)
                    {
                        view.err_empty_table("This");
                    }
                    else
                    {
                        view.print(searches);
                    }
                }
                else if (search == 3)
                {
                    string d_name = view.search_d_name();
                    while (d_name.Length == 0)
                    {
                        view.err_empty("Substring");
                        d_name = view.search_d_name();
                    }
                    string m_name = view.search_m_name();
                    while (m_name.Length == 0)
                    {
                        view.err_empty("Substring");
                        m_name = view.search_m_name();
                    }
                    int s_year = 0;
                    while (!Int32.TryParse(view.search_s_year(), out s_year))
                    {
                        view.err_number("Input");
                    }
                    int e_year = 0;
                    while (!Int32.TryParse(view.search_e_year(), out e_year))
                    {
                        view.err_number("Input");
                    }
                    string searches = model.search_3(d_name, m_name, s_year, e_year);
                    if (searches.Length == 0)
                    {
                        view.err_empty_table("This");
                    }
                    else
                    {
                        view.print(searches);
                    }
                }
                else if (search == 4)
                {
                    break;
                }
                else
                {
                    view.err_wrong_option();
                }
            }
        }
        #endregion
    }
}
