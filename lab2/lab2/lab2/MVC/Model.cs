﻿using System;
using System.Diagnostics;
using Npgsql;

namespace lab2.MVC
{
    class Model
    {
        private NpgsqlConnection db;
        public Model()
        {
            if (db == null)
            {
                db = new NpgsqlConnection("Host = localhost; Username = postgres; Password = boogieman13; Database = stopen_db");
                db.Open();

                using var cmd = new NpgsqlCommand("SELECT version()", db);

                var version = cmd.ExecuteScalar().ToString();
                Console.WriteLine($"PostgreSQL version: {version}");
            }
        }

        #region movies
        public string movie_print()
        {
            using var cmd = new NpgsqlCommand("SELECT * from movie ORDER BY id ASC  ", db);
            using NpgsqlDataReader rdr = cmd.ExecuteReader();
            string movies = "";
            while (rdr.Read())
            {
                movies += rdr.GetInt32(0);
                if (movies.Length == 0)
                {
                    return movies;
                }
                movies += ". Name: ";
                movies += rdr.GetString(1);
                movies += "   Genre: ";
                movies += rdr.GetString(2);
                movies += "   Country: ";
                movies += rdr.GetString(3);
                movies += "   Duration: ";
                movies += rdr.GetInt32(4);
                movies += "   Director ID: ";
                if (rdr.IsDBNull(5))
                {
                    movies += "NULL";
                }
                else
                {
                    movies += rdr.GetInt32(5);
                }
                movies += "\n";
            }
            return movies;
        }
        public string movie_get_by_id(int movie_id)
        {
            using var cmd = new NpgsqlCommand("SELECT * FROM movie WHERE id=" + movie_id, db);
            using NpgsqlDataReader rdr = cmd.ExecuteReader();
            string movie = "";
            while (rdr.Read())
            {
                movie += rdr.GetInt32(0);
                if (movie.Length == 0)
                {
                    return movie;
                }
                movie += ". Name: ";
                movie += rdr.GetString(1);
                movie += "   Genre: ";
                movie += rdr.GetString(2);
                movie += "   Country: ";
                movie += rdr.GetString(3);
                movie += "   Duration: ";
                movie += rdr.GetInt32(4);
                movie += "   Director ID: ";
                if (rdr.IsDBNull(5))
                {
                    movie += "NULL";
                }
                else
                {
                    movie += rdr.GetInt32(5);
                }
                movie += "\n";
                movie += "\n";
            }
            return movie;
        }
        public int movie_add(string m_name, string m_genre, string m_country, int m_duration, int m_id)
        {
            using var cmd = new NpgsqlCommand("INSERT INTO movie(name, genre, country, duration, director_id) VALUES(@name, @genre, @country, @duration, @director_id)", db);
            cmd.Parameters.AddWithValue("name", m_name);
            cmd.Parameters.AddWithValue("genre", m_genre);
            cmd.Parameters.AddWithValue("country", m_country);
            cmd.Parameters.AddWithValue("duration", m_duration);
            cmd.Parameters.AddWithValue("director_id", m_id);
            cmd.Prepare();
            cmd.ExecuteNonQuery();
            using var cmd2 = new NpgsqlCommand("SELECT id FROM movie WHERE id = (SELECT MAX(id) from movie)", db);
            using NpgsqlDataReader rdr = cmd2.ExecuteReader();
            int new_id = 0;
            while (rdr.Read())
            {
                new_id = rdr.GetInt32(0);
            }
            return new_id;
        }
        public void movie_delete(int movie_id)
        {
            using var cmd2 = new NpgsqlCommand("DELETE from nomination WHERE movie_id = " + movie_id, db);
            cmd2.ExecuteNonQuery();
            using var cmd = new NpgsqlCommand("DELETE from movie WHERE id = " + movie_id, db);
            cmd.ExecuteNonQuery();
        }
        public void movie_edit(string m_name, string m_genre, string m_country, int m_duration, int m_id, int movie_id)
        {
            using var cmd = new NpgsqlCommand("UPDATE movie SET name = @m_name, genre = @m_genre, country = @m_country, duration = @m_duration, director_id = @m_id WHERE id = " + movie_id, db);
            cmd.Parameters.AddWithValue("@m_name", m_name);
            cmd.Parameters.AddWithValue("@m_genre", m_genre);
            cmd.Parameters.AddWithValue("@m_country", m_country);
            cmd.Parameters.AddWithValue("@m_duration", m_duration);
            cmd.Parameters.AddWithValue("@m_id", m_id);
            cmd.ExecuteNonQuery();
        }

        public void movie_generation (int num)
        {
            using var cmd = new NpgsqlCommand("INSERT INTO movie (name, genre, country, duration, director_id) SELECT chr(trunc(65 + random()*25)::int) || chr(trunc(97 + random()*25)::int) || chr(trunc(97 + random()*25)::int) || chr(trunc(97 + random()*25)::int), chr(trunc(97 + random()*25)::int) || chr(trunc(97 + random()*25)::int) || chr(trunc(97 + random()*25)::int) || chr(trunc(97 + random()*25)::int), chr(trunc(65 + random()*25)::int) || chr(trunc(97 + random()*25)::int) ||  chr(trunc(97 + random()*25)::int), trunc(random() * 500 + 20), gen_director_id() FROM generate_series(1, @num)", db);
            cmd.Parameters.AddWithValue("@num", num);
            cmd.ExecuteNonQuery();
        }
        #endregion

        #region directors
        public string director_print()
        {
            using var cmd = new NpgsqlCommand("SELECT * from director ORDER BY id ASC  ", db);
            using NpgsqlDataReader rdr = cmd.ExecuteReader();
            string directors = "";
            while (rdr.Read())
            {
                directors += rdr.GetInt32(0);
                if (directors.Length == 0)
                {
                    return directors;
                }
                directors += ". Name: ";
                directors += rdr.GetString(1);
                directors += "   Country: ";
                directors += rdr.GetString(2);
                directors += "\n";
            }
            return directors;
        }
        public string director_get_by_id(int dir_id)
        {
            using var cmd = new NpgsqlCommand("SELECT * FROM director WHERE id=" + dir_id, db);
            using NpgsqlDataReader rdr = cmd.ExecuteReader();
            string director = "";
            while (rdr.Read())
            {
                director += rdr.GetInt32(0);
                if (director.Length == 0)
                {
                    return director;
                }
                director += ". Name: ";
                director += rdr.GetString(1);
                director += "   Country: ";
                director += rdr.GetString(2);
                director += "\n";
            }
            return director;
        }
        public int director_add(string d_name, string d_country)
        {
            using var cmd = new NpgsqlCommand("INSERT INTO director(name, country_of_birth) VALUES(@name, @country_of_birth)", db);
            cmd.Parameters.AddWithValue("name", d_name);
            cmd.Parameters.AddWithValue("country_of_birth", d_country);
            cmd.Prepare();
            cmd.ExecuteNonQuery();
            using var cmd2 = new NpgsqlCommand("SELECT id FROM director WHERE id = (SELECT MAX(id) from director)", db);
            using NpgsqlDataReader rdr = cmd2.ExecuteReader();
            int new_id = 0;
            while (rdr.Read())
            {
                new_id = rdr.GetInt32(0);
            }
            return new_id;
        }
        public void director_delete(int director_id)
        {
            using var cmd2 = new NpgsqlCommand("UPDATE movie SET director_id = NULL WHERE id in (SELECT id FROM movie WHERE director_id = @dir_id)", db);
            cmd2.Parameters.AddWithValue("@dir_id", director_id);
            cmd2.ExecuteNonQuery();
            using var cmd = new NpgsqlCommand("DELETE from director WHERE id = " + director_id, db);
            cmd.ExecuteNonQuery();
        }
        public void director_edit(string d_name, string d_country, int director_id)
        {
            using var cmd = new NpgsqlCommand("UPDATE director SET name = @d_name, country_of_birth = @d_country WHERE id = " + director_id, db);
            cmd.Parameters.AddWithValue("@d_name", d_name);
            cmd.Parameters.AddWithValue("@d_country", d_country);
            cmd.ExecuteNonQuery();
        }
        public void director_generation(int num)
        {
            using var cmd = new NpgsqlCommand("INSERT INTO director (name, country_of_birth) SELECT chr(trunc(65 + random()*25)::int) || chr(trunc(97 + random()*25)::int) || chr(trunc(97 + random()*25)::int) || chr(trunc(97 + random()*25)::int) || chr(trunc(97 + random()*25)::int), chr(trunc(65 + random()*25)::int) || chr(trunc(97 + random()*25)::int) || chr(trunc(97 + random()*25)::int) || chr(trunc(97 + random()*25)::int) FROM generate_series(1, @num)", db);
            cmd.Parameters.AddWithValue("@num", num);
            cmd.ExecuteNonQuery();
        }
        #endregion

        #region awards
        public string award_print()
        {
            using var cmd = new NpgsqlCommand("SELECT * from award ORDER BY id ASC  ", db);
            using NpgsqlDataReader rdr = cmd.ExecuteReader();
            string awards = "";
            while (rdr.Read())
            {
                awards += rdr.GetInt32(0);
                if (awards.Length == 0)
                {
                    return awards;
                }
                awards += ". Category: ";
                awards += rdr.GetString(1);
                awards += "   Year: ";
                awards += rdr.GetInt32(2);
                awards += "\n";
            }
            return awards;
        }
        public string award_get_by_id(int award_id)
        {
            using var cmd = new NpgsqlCommand("SELECT * FROM award WHERE id=" + award_id, db);
            using NpgsqlDataReader rdr = cmd.ExecuteReader();
            string award = "";
            while (rdr.Read())
            {
                award += rdr.GetInt32(0);
                if (award.Length == 0)
                {
                    return award;
                }
                award += ". Category: ";
                award += rdr.GetString(1);
                award += "   Year: ";
                award += rdr.GetInt32(2);
                award += "\n";
            }
            return award;
        }
        public int award_add(string a_category, int a_year)
        {
            using var cmd = new NpgsqlCommand("INSERT INTO award(category, year) VALUES(@category, @year)", db);
            cmd.Parameters.AddWithValue("category", a_category);
            cmd.Parameters.AddWithValue("year", a_year);
            cmd.Prepare();
            cmd.ExecuteNonQuery();
            using var cmd2 = new NpgsqlCommand("SELECT id FROM award WHERE id = (SELECT MAX(id) from award)", db);
            using NpgsqlDataReader rdr = cmd2.ExecuteReader();
            int new_id = 0;
            while (rdr.Read())
            {
                new_id = rdr.GetInt32(0);
            }
            return new_id;
        }
        public void award_delete(int award_id)
        {
            using var cmd2 = new NpgsqlCommand("DELETE from nomination WHERE award_id = " + award_id, db);
            cmd2.ExecuteNonQuery();
            using var cmd = new NpgsqlCommand("DELETE from award WHERE id = " + award_id, db);
            cmd.ExecuteNonQuery();
        }
        public void award_edit(string a_category, int a_year, int award_id)
        {
            using var cmd = new NpgsqlCommand("UPDATE award SET category = @a_category, year = @a_year WHERE id = " + award_id, db);
            cmd.Parameters.AddWithValue("@a_category", a_category);
            cmd.Parameters.AddWithValue("@a_year", a_year);
            cmd.ExecuteNonQuery();
        }
        public void award_generation(int num)
        {
            using var cmd = new NpgsqlCommand("INSERT INTO award (category, year) SELECT chr(trunc(65 + random()*25)::int) || chr(trunc(97 + random()*25)::int) || chr(trunc(97 + random()*25)::int) || chr(trunc(97 + random()*25)::int) || chr(trunc(97 + random()*25)::int), trunc(1900 + random() * 2021)::int  FROM generate_series(1, @num)", db);
            cmd.Parameters.AddWithValue("@num", num);
            cmd.ExecuteNonQuery();
        }
        #endregion

        #region movie_award
        public string movie_award_print()
        {
            using var cmd = new NpgsqlCommand("SELECT movie.id AS movie_id, movie.name, nomination.award_id, award.category from movie join nomination on (movie.id = nomination.movie_id)join award on (nomination.award_id = award.id);", db);
            using NpgsqlDataReader rdr = cmd.ExecuteReader();
            string movies_awards = "";
            while (rdr.Read())
            {
                movies_awards += rdr.GetInt32(0);
                if (movies_awards.Length == 0)
                {
                    return movies_awards;
                }
                movies_awards += ". Movie Name: ";
                movies_awards += rdr.GetString(1);
                movies_awards += " <---> ";
                movies_awards += rdr.GetInt32(2);
                movies_awards += ". Award Category: ";
                movies_awards += rdr.GetString(3);
                movies_awards += "\n";
            }
            return movies_awards;
        }
        public void movie_award_add(int m_id, int a_id)
        {
            using var cmd = new NpgsqlCommand("INSERT INTO nomination(movie_id, award_id) VALUES((SELECT id from movie where id = @m_id), (SELECT id from award where id = @a_id))", db);
            cmd.Parameters.AddWithValue("m_id", m_id);
            cmd.Parameters.AddWithValue("a_id", a_id);
            cmd.Prepare();
            cmd.ExecuteNonQuery();
        }
        public string movie_award_delete(int m_id, int a_id)
        {
            using var cmd = new NpgsqlCommand("DELETE from nomination WHERE movie_id = @m_id AND award_id = @a_id ", db);
            cmd.Parameters.AddWithValue("m_id", m_id);
            cmd.Parameters.AddWithValue("a_id", a_id);
            return cmd.ExecuteNonQuery().ToString();
            
        }
        public void movie_award_generation(int num)
        {
            using var cmd = new NpgsqlCommand("INSERT INTO nomination (movie_id, award_id) SELECT gen_movie_id(), gen_award_id()  FROM generate_series(1, @num)", db);
            cmd.Parameters.AddWithValue("@num", num);
            cmd.ExecuteNonQuery();
        }
        #endregion
        #region search
        public string search_1(int s_dur, int e_dur, int s_id, int e_id, int s_year, int e_year)
        {
            using var cmd = new NpgsqlCommand("SELECT movie.id AS movie_id, movie.name, movie.duration, movie.director_id, nomination.award_id, award.category, award.year from movie join nomination  on (movie.id = nomination.movie_id)join award on (nomination.award_id = award.id) WHERE movie.duration BETWEEN @s_dur AND @e_dur AND movie.director_id BETWEEN @s_id AND @e_id AND award.year BETWEEN @s_year AND @e_year", db);
            cmd.Parameters.AddWithValue("s_dur", s_dur);
            cmd.Parameters.AddWithValue("e_dur", e_dur);
            cmd.Parameters.AddWithValue("s_id", s_id);
            cmd.Parameters.AddWithValue("e_id", e_id);
            cmd.Parameters.AddWithValue("s_year", s_year);
            cmd.Parameters.AddWithValue("e_year", e_year);
            TimeSpan ts = DateTime.Now.TimeOfDay;
            var sw = new Stopwatch();
            sw.Start();
            using NpgsqlDataReader rdr = cmd.ExecuteReader();
            string search = "";
            while (rdr.Read())
            {
                search += rdr.GetInt32(0);
                if (search.Length == 0)
                {
                    return search;
                }
                search += ". Movie Name: ";
                search += rdr.GetString(1);
                search += "   Duration:";
                search += rdr.GetInt32(2);
                search += "   Director ID: ";
                search += rdr.GetInt32(3);
                search += " <---> ";
                search += rdr.GetInt32(4);
                search += ".  Award Category: ";
                search += rdr.GetString(5);
                search += "   Year ";
                search += rdr.GetInt32(6);
                search += "\n";
            }
            var elapsed = sw.ElapsedMilliseconds;
            Console.WriteLine($"Query Executed and Results Returned in 0.{elapsed.ToString()}sec");
            return search;
        }
        public string search_2(string d_name, int e_dur, int s_dur, string m_name)
        {
            using var cmd = new NpgsqlCommand("SELECT director.id, director.name, movie.id, movie.name, movie.duration from director join movie  on (director.id = movie.director_id) WHERE director.name like '%"+d_name+ "%' AND movie.duration BETWEEN @s_dur and @e_dur AND movie.name like '%" + m_name+"%'", db);
            cmd.Parameters.AddWithValue("e_dur", e_dur);
            cmd.Parameters.AddWithValue("s_dur", s_dur);
            var sw = new Stopwatch();
            sw.Start();
            using NpgsqlDataReader rdr = cmd.ExecuteReader();
            string search = "";
            while (rdr.Read())
            {
                search += rdr.GetInt32(0);
                if (search.Length == 0)
                {
                    return search;
                }
                search += ". Director Name: ";
                search += rdr.GetString(1);
                search += " ---> ";
                search += rdr.GetInt32(2);
                search += ". Movie Name: ";
                search += rdr.GetString(3);
                search += "   Duration:";
                search += rdr.GetInt32(4);
                search += "\n";
            }
            var elapsed = sw.ElapsedMilliseconds;
            Console.WriteLine($"Query Executed and Results Returned in 0.{elapsed.ToString()}sec");
            return search;
        }
        public string search_3(string d_name, string m_name, int s_year, int e_year)
        {
            using var cmd = new NpgsqlCommand("SELECT movie.id, movie.name, nomination.award_id, award.category, award.year, director.id, director.name from movie join nomination on (movie.id = nomination.movie_id) join award on (nomination.award_id = award.id)join director on (movie.director_id = director.id) WHERE director.name LIKE '%"+d_name+ "%' AND movie.name like '%" + m_name + "%' AND award.year BETWEEN @s_year AND @e_year", db);
            cmd.Parameters.AddWithValue("s_year", s_year);
            cmd.Parameters.AddWithValue("e_year", e_year);
            var sw = new Stopwatch();
            sw.Start();
            using NpgsqlDataReader rdr = cmd.ExecuteReader();
            string search = "";
            while (rdr.Read())
            {
                search += rdr.GetInt32(0);
                if (search.Length == 0)
                {
                    return search;
                }
                search += ". Movie Name: ";
                search += rdr.GetString(1);
                search += " <---> ";
                search += rdr.GetInt32(2);
                search += ". Award Category: ";
                search += rdr.GetString(3);
                search += "   Year: ";
                search += rdr.GetInt32(4);
                search += " ---> ";
                search += rdr.GetInt32(2);
                search += ". Director Name: ";
                search += rdr.GetString(3);
                search += "\n";
            }
            var elapsed = sw.ElapsedMilliseconds;
            Console.WriteLine($"Query Executed and Results Returned in 0.{elapsed.ToString()}sec");
            return search;
        }
        #endregion
    }
}
