Name: Створення додатку бази даних, орієнтованого на взаємодію з СУБД PostgreSQL
Data Base:
movie
 INT id <PK>
 TEXT name
 TEXT genre
 TEXT country
 INT duration
 INT director_id <FK>
director
 INT id <PK>
 TEXT name
 TEXT country_of_birth
award
 INT id <PK>
 TEXT category
 INT year
nomination
 INT movie_id <FK>
 INT award_id <FK>

movie -> director
movie <-> award